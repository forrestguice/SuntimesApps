# SuntimesApps

**Suntimes Apps** is an F-Droid compatible repository for [Suntimes](https://github.com/forrestguice/SuntimesWidget) related apps.

To add to your F-Droid client, [https://forrestguice.codeberg.page/SuntimesApps/repo](https://forrestguice.codeberg.page/SuntimesApps/repo).

This repository is also available at [https://forrestguice.github.io/SuntimesApps/repo](https://forrestguice.github.io/SuntimesApps/repo).


## Donations

Do you find value in this software? Please pay as you feel.

[![paypal](https://www.paypalobjects.com/webstatic/en_US/i/btn/png/silver-rect-paypal-26px.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NZJ5FJBCKY6K2) 

<a href="https://liberapay.com/forrestguice/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>


